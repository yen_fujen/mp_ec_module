/**
 * 載入相關套件
 */
import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  ActivityIndicator,
  Text,
  ViewPropTypes,
} from 'react-native';
import {theme} from '../configs';

/**
 * Loading Class
 */
class Loading extends React.PureComponent {
  /**
   * 屬性驗證
   * https://facebook.github.io/react/docs/reusable-components-zh-CN.html
   */
  static propsTypes = {
    iconWidth: PropTypes.number,
    iconHeight: PropTypes.number,
    color: PropTypes.string,
    fontSize: PropTypes.number,
    iconStyle: ViewPropTypes.style,
    textStyle: ViewPropTypes.style,
    text: PropTypes.string,
  };

  /**
   * 建立元件預設值
   */
  static defaultProps = {
    iconWidth: 140,
    iconHeight: 140,
    color: '#FFFFFF',
    fontSize: 16,
    iconStyle: {},
    textStyle: {},
    text: 'loading',
  };

  /**
   * Constructor
   */
  constructor(props) {
    super(props);

    this.state = {};
  }

  /**
   * 渲染畫面
   */
  render() {
    const style = StyleSheet.flatten([
      styles.content,
      {
        height: this.props.iconHeight,
        width: this.props.iconWidth,
        top: (theme.deviceHeight - this.props.iconHeight) / 2,
        left: (theme.deviceWidth - this.props.iconWidth) / 2,
      },
    ]);

    const textStyle = StyleSheet.flatten([
      styles.text,
      {
        color: this.props.color,
        fontSize: this.props.fontSize,
        lineHeight: +this.props.fontSize + 6,
      },
      this.props.textStyle,
    ]);

    return (
      <View style={styles.container}>
        <View style={style}>
          <ActivityIndicator
            color={this.props.color}
            style={this.props.iconStyle}
            size="large"
          />
          <Text style={textStyle}>{this.props.text}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.backdrop,
    width: theme.deviceWidth,
    height: theme.deviceHeight,
    position: 'absolute',
    top: 0,
    left: 0,
  },
  content: {
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    flexDirection: 'column',
  },
  text: {
    textAlign: 'center',
  },
});

export default Loading;
