import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

export default class SNBtn extends React.PureComponent {
  static propTypes = {
    paired: PropTypes.bool.isRequired,
    sn: PropTypes.string,
    ip: PropTypes.string,
    mac: PropTypes.string,
    onPress: PropTypes.func.isRequired,
  };

  static defaultProps = {
    paired: false,
    mac: null,
  };

  onPress = () => this.props.onPress({sn: this.props.sn, ip: this.props.ip});

  render() {
    let color = '#01a8b8';
    let icon = 'phonelink-ring';
    let state = '未配對';

    if (this.props.paired === true) {
      color = '#959595';
      icon = 'phonelink-lock';
      state = '已配對';
    }

    const style = StyleSheet.flatten([styles.icon, {color}]);
    const titleStyle = StyleSheet.flatten([styles.title, {color}]);
    const detailStyle = StyleSheet.flatten([styles.detail, {color}]);

    return (
      <TouchableOpacity style={styles.container} onPress={this.onPress}>
        <View style={styles.content}>
          <MaterialIcon name={icon} size={18} style={style} />
          <Text style={titleStyle}>
            {this.props.sn}
            <Text style={detailStyle}>({state})</Text>
          </Text>
        </View>
        <Text style={detailStyle}>{this.props.mac}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    borderColor: '#adedf1',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 16,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#d8f6f8',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    flexDirection: 'row',
  },
  icon: {
    fontSize: 20,
    marginTop: 3,
  },
  title: {
    color: '#01a8b8',
    fontSize: 20,
    lineHeight: 28,
  },
  detail: {
    fontSize: 14,
  },
});
