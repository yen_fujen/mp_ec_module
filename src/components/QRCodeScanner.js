import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View} from 'react-native';
import {RNCamera} from 'react-native-camera';
import BarCodeMask from 'react-native-barcode-mask';

/**
 *  base from react-native-qrcode-reader changed
 *  url:https://github.com/lazaronixon/react-native-qrcode-reader/blob/master/README.md
 *
 * */
export default class QRCodeScanner extends React.PureComponent {
  static propTypes = {
    onSuccess: PropTypes.func.isRequired,
    isCameraReady: PropTypes.bool,
    canReadBarCode: PropTypes.bool,
  };

  static defaultProps = {
    backdropColor: 'rgba(0,35,50,0.5)',
  };

  /**
   * 返還的資料格式如下:
   *   {
   *     type: "QR_CODE",
   *     data: "AH8AAAF201",
   *     bounds: [
   *      { x: '126.601162', y: '236.667220' }
   *     ],
   *   }
   */
  onBarCodeRead = result => {
    if (this.props.canReadBarCode) {
      this.props.onSuccess(result.data);
    }
  };

  renderContent = () => (
    <BarCodeMask edgeColor="#01a8b8" showAnimatedLine={false} />
  );

  render() {
    return (
      <View style={styles.container}>
        {this.props.isCameraReady ? (
          <RNCamera
            style={styles.camera}
            type={RNCamera.Constants.Type.back}
            flashMode={RNCamera.Constants.FlashMode.auto}
            onBarCodeRead={this.onBarCodeRead}
            captureAudio={false}>
            {this.renderContent()}
          </RNCamera>
        ) : (
          this.renderContent()
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'transparent',
  },

  camera: {
    flexGrow: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});
