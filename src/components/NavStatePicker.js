import React from 'react';
import eko from '../services/eko';

export const NavStatePicker = WrappedComponent => {
  class HOC extends React.Component {
    constructor(props) {
      super(props);
      this.sn = props.navigation.getParam('sn');
      this.mpReport = props.navigation.getParam('mpReport');
      this.scannedSn = props.navigation.getParam('scannedSn');
      // haveAnyTestsFailed 的目的為確認在 mp 過程中是否有發生過錯誤
      // 如果 haveAnyTestsFailed 有，在做完 LED 開關測試和 Reset 測試後就會接著送出 mp 報告（省略掃描 QR Code 這一步）
      this.haveAnyTestsFailed = props.navigation.getParam(
        'haveAnyTestsFailed',
        false,
      );
    }

    mpRequest = action =>
      eko.request({
        method: 'p_mp',
        params: {action},
        sn: this.sn,
      });

    sendMpReport = result =>
      eko.request({
        method: 'mp_report',
        params: {
          testcases: this.mpReport,
          sn: this.sn,
          created: new Date().getTime(),
          result,
          type: 'peripheral_mp_report',
        },
        // sn: this.sn,
      });

    render() {
      return (
        <WrappedComponent
          {...this.props}
          sn={this.sn}
          mpReport={this.mpReport}
          mpRequest={this.mpRequest}
          scannedSn={this.scannedSn}
          sendMpReport={this.sendMpReport}
          haveAnyTestsFailed={this.haveAnyTestsFailed}
        />
      );
    }
  }

  HOC.displayName = `[HOC] (${getDisplayName(WrappedComponent)})`;

  return HOC;
};

const getDisplayName = WrappedComponent =>
  WrappedComponent.displayName || WrappedComponent.name || 'Component';
