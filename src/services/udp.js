import {Platform} from 'react-native';
import {isNil} from 'ramda';
import dgram from 'react-native-udpsocket';
import EventEmitter from 'events';

const PORT = 2324;
// const PORT = 9898;
// 後端的機制是使用 UDP broadcast 所以使用 multicast 的 ip 不見得收的到訊息
const BROADCAST_IP = '255.255.255.255'; // udp broadcast ip
const MESSAGE = 'HiEKM;a;';
// const MESSAGE = 'HelloSentiReef;all;';

const toByteArray = obj => {
  const uint = new Uint8Array(obj.length);
  for (let i = 0, l = obj.length; i < l; i++) {
    uint[i] = obj.charCodeAt(i);
  }
  return new Uint8Array(uint);
};

const bin2String = array => {
  let result = '';
  for (let i = 0; i < array.length; i++) {
    result += String.fromCharCode(parseInt(array[i], 10));
  }
  return result;
};

class UDP extends EventEmitter {
  _create = () => {
    this.socket = dgram.createSocket('udp4');
    if (!isNil(window)) {
      window.udp = this.socket;
    }
  };

  constructor() {
    super();

    this.isBind = true;
    this._create();
  }

  getInstance = () => this.socket;

  open = () =>
    new Promise((resolve, reject) => {
      try {
        if (isNil(this.socket)) {
          this._create();
        }

        // 啟動 UDP
        this.socket.once('listening', () => {
          if (Platform.OS === 'ios') {
            // allow udp package send to broadcast ip
            this.socket.setBroadcast(true);
          }
          const buf = toByteArray(MESSAGE);
          this.socket.send(buf, 0, buf.length, PORT, BROADCAST_IP, err => {
            if (err) {
              throw err;
            }
            console.log('[啟動 UDP] message was sent');
          });
        });

        // 設定監聽埠
        this.socket.bind(PORT, error => {
          if (!error) {
            console.log(`[UDP / 設定監聽埠] udp bound to port ${PORT}`);
            this.isBind = false;
            resolve();
          } else {
            console.log('[UDP / 設定監聽埠] bound to port have error!', error);
            reject(error);
          }
        });

        // 監聽廣播事件
        this.socket.on('message', (chunk, rinfo) => {
          const msg = bin2String(chunk);
          // 當收到 UDP 回應的訊息不是 'HiEKM' 的話，就表示是機器
          if (!msg.includes('HiEKM')) {
            const entry = JSON.parse(msg);
            entry.ip = rinfo.address;
            this.emit('message', entry);
          }
        });

        this.socket.on('error', () => reject('udp error'));
      } catch (error) {
        reject(error);
      }
    });

  write = message => {
    if (this.isBind === false) {
      const buf = toByteArray(message);
      this.socket.send(buf, 0, buf.length, PORT, BROADCAST_IP, err => {
        if (err) {
          console.log(`send ${BROADCAST_IP}: ${PORT}, fail:`, err);
        }
      });
    } else {
      console.log('[UDP]: udp write have error!!');
    }
  };

  close = () => {
    this.socket.close();
    this.socket.removeAllListeners();
    this.socket = null;
    if (isNil(window)) {
      window.udp = null;
    }
  };
}

const udp = new UDP();
export default udp;
