import {isEmpty, isNil} from 'ramda';
import {Commx} from 'commx';
import Config from 'react-native-config';
import MqttInit from 'react_native_mqtt';
import AsyncStorage from '@react-native-community/async-storage';

const STORAGE_MAXIMUM_CAPACITY = 100000;
// const TIMEOUT_LIMIT = 30000; // 連線時間限制，預設 30 秒
const EXPIRES_TIME = 1000 * 3600 * 24; // Storage 保存時間，預設 1 天

const SERVICE = {
  MEMBER: 'member',
  PERIPHERAL: 'peripheral',
  EKO: 'eko',
};

// 遠端 mqtt 服務網址
const CONFIGS = {
  host: Config.REMOTE_EKO_MQTT,
  port: Config.REMOTE_EKO_PORT,
  protocol: Config.REMOTE_EKO_PROTOCOL,
  mqtt: {
    username: Config.REMOTE_EKO_MQTT_USERNAME,
    password: Config.REMOTE_EKO_MQTT_PASSWORD,
  },
};

class EKO {
  _create = () => {
    if (!isNil(this.commx)) {
      this.close();
    }

    this.commx = new Commx(CONFIGS);
    if (!isNil(window)) {
      window.commx = this.commx;
    }
  };

  constructor() {
    /**
     * see https://github.com/sunnylqm/react-native-storage for more details
     */
    MqttInit({
      size: STORAGE_MAXIMUM_CAPACITY,
      storageBackend: AsyncStorage,
      defaultExpires: EXPIRES_TIME,
      enableCache: true,
      reconnect: true,
      sync: {},
    });

    /**
     * 這邊先執行 comm.open 的目的是為了建立 server (member) 的連線
     * commx 的 server 架構是採用 mqtt，只會建立一條連線與 server 進行溝通
     * server 端包含 member 和 remote 二個服務
     */
    if (isNil(this.commx)) {
      this._create();
    }
  }

  /**
   * 取得服務類型
   */
  services = () => SERVICE;

  /**
   * 裝置連線
   * @param {Array|Object} devices 連接裝置
   */
  configure = ({email, sid}) => {
    this.commx.setEmail(email);
    this.commx.setSid(sid);
  };

  /**
   * 啟動 Commx
   */
  open = () =>
    new Promise((resolve, reject) => {
      try {
        if (isNil(this.commx)) {
          this._create();
        }

        // 連線時間限制設定
        // setTimeout(() => {
        //   reject(new Error('Socket connection timeout'));
        // }, TIMEOUT_LIMIT);

        this.commx.on('connect', service => {
          console.log('[debug:commx] eko service connected! ', service);
          resolve(service);
        });

        this.commx.on('disconnect', service => {
          console.log(
            '[debug:commx] eko service disconnect! service:',
            service,
          );
          reject(service);
        });

        this.commx.open();
      } catch (error) {
        console.log('[debug:commx] commx open fail. Error message: ', error);
        reject(error);
      }
    });

  getInstance = () => this.commx;

  /**
   * 關閉連線
   */
  close = () =>
    new Promise((resolve, reject) => {
      try {
        this.commx.close();
        this.commx = null;
        if (!isNil(window)) {
          window.commx = null;
        }
        console.log('[debug:commx] commx disconnect success.');
        resolve();
      } catch (error) {
        console.log(
          '[debug:commx] commx disconnect fail. Error message: ',
          error,
        );
        reject(error);
      }
    });

  /**
   * 單一或多個機器執行連線
   * @param {Object|Array} device
   */
  connect = device =>
    new Promise((resolve, reject) => {
      try {
        if (isEmpty(device)) {
          reject(
            new Error(
              'No device! "connect" function must have one connected device',
            ),
          );
        }

        const devices = !Array.isArray(device) ? [device] : devices;

        console.log('[debug:commx] 單一或多個機器執行連線:', device, devices);

        // 連線操作不允許超過指定秒數
        // setTimeout(reject(new Error('Socket connection timeout')), TIMEOUT_LIMIT);

        // 注意: addDevices 會啟動 open，所以不需要再另外呼叫 commx.open()
        this.commx.addDevices(devices);

        // Commx Service 分成 member 和 peripheral 二個服務
        this.commx.on('connect', service => {
          if (
            service.type === SERVICE.PERIPHERAL ||
            service.type === SERVICE.EKO
          ) {
            resolve(service);
          }
        });
      } catch (error) {
        console.log('[debug:commx] commx connect fail. Error message: ', error);
        reject(error);
      }
    });

  /**
   * 重新連線
   */
  reconnect = () =>
    new Promise(resolve => {
      // setTimeout(reject(new Error('Socket connection timeout')), TIMEOUT_LIMIT);

      // Commx 的 reconnectAll 是將目前所有服務斷線，然後再啟動(open)一次，當中也包含 member 服務
      this.commx.reconnectAll();
      this.commx.once('connect', service => {
        resolve(service);
      });
    });

  /**
   * 斷線
   */
  disconnect = () =>
    new Promise(resolve => {
      this.commx.close();
      this.commx.once('disconnect', service => {
        resolve(service);
      });
    });

  /**
   * 斷開單一或多個機器連線
   */
  disconnectDevice = sn =>
    new Promise((resolve, reject) => {
      if (isEmpty(sn)) {
        reject(
          new Error(
            'No device! "disconnect" function must have one connected device sn',
          ),
        );
      }

      const snList = !Array.isArray(sn) ? [sn] : sn;
      console.log(
        `[debug:commx] commx disconnect device "${snList.join(',')}" success.`,
      );
      this.commx.removeDevices(snList);
      resolve(snList);
    });

  /**
   * 呼叫 Commx RPC 命令和硬體進行溝通
   *
   * 如果是中控器情況，不需要再傳入 sn 參數
   * 如果是一般週邊裝置，需要再傳入 sn 參數，指定是哪一台機器需要建立溝通
   *
   * @param {Object} { method: String, params: Object, sn: Array }
   * @return Promise
   */
  request = ({method, sn = '', params = {}, options = {}, version = 1}) => {
    if (isNil(sn)) {
      return Promise.reject(
        new Error('[socket.js]: sn is required in request function'),
      );
    }
    if (isNil(method)) {
      return Promise.reject(
        new Error('[socket.js]: method is required in request function'),
      );
    }

    const request = {
      name: method,
      parm: params,
      version,
      options,
    };

    let rpcParams = {};

    // 裝置操作，需要有 SN
    rpcParams = {sn, request};
    console.log(
      `[debug:commx] send device rpc request, method: "${method}", parm:`,
      rpcParams,
    );
    return this.commx.rpc(rpcParams);
  };
}

const eko = new EKO();
export default eko;
