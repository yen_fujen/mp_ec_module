import {Alert} from 'react-native';
import throttle from 'lodash/throttle';
import {
  check,
  request,
  openSettings,
  PERMISSIONS,
  RESULTS,
} from 'react-native-permissions';

/**
 * @param {Function} fn - to throttle calls within 1 sec
 * @return {Function}
 */
export const throttler = fn =>
  throttle(fn, 1000, {leading: true, trailing: false});

export const requestCameraPermission = () =>
  request(PERMISSIONS.ANDROID.CAMERA).then(result => {
    if (result === RESULTS.DENIED) {
      throw new Error(RESULTS.DENIED);
    }
    return result;
  });

export const confirmCameraPermission = async () => {
  const permission = await check('camera');
  if (permission === 'authorized') {
    return permission;
  }
  if (permission === 'undetermined') {
    throw new Error(permission);
  }
  return new Promise((resolve, reject) =>
    Alert.alert('需要相機權限', '允許使用相機來掃描 QR Code', [
      {
        text: '取消',
        onPress: () => reject(permission),
      },
      {
        text: '設定',
        onPress: () => {
          resolve(permission);
          openSettings();
        },
      },
    ]),
  );
};

export const checkCameraPermission = async () => {
  return check(PERMISSIONS.ANDROID.CAMERA)
    .then(result => {
      switch (result) {
        case RESULTS.UNAVAILABLE:
          console.log(
            'This feature is not available (on this device / in this context)',
          );
          return result;
        case RESULTS.DENIED:
          console.log(
            'The permission has not been requested / is denied but requestable',
          );
          return result;
        case RESULTS.GRANTED:
          console.log('The permission is granted');
          return result;
        case RESULTS.BLOCKED:
          console.log('The permission is denied and not requestable anymore');
          return result;
      }
    })
    .catch(error => {
      console.warn('[Error / Android / Check Camera Permission]', error);
    });
};
