import PropTypes from 'prop-types';

export {default as theme} from './theme';

export const NavigationPropTypes = PropTypes.shape({
  navigate: PropTypes.func.isRequired,
  goBack: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
  setParams: PropTypes.func.isRequired,
  getParam: PropTypes.func.isRequired,
  popToTop: PropTypes.func,
  dismiss: PropTypes.func,
}).isRequired;
