import {Dimensions, PixelRatio} from 'react-native';

const {width, height} = Dimensions.get('window');

// Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

// 手機目前倍率
const pixelRatio = PixelRatio.get();
const adjustedWidth = width * pixelRatio;
const adjustedHeight = height * pixelRatio;
// 依照傳入比例和螢幕總長寬計算對應長寬
const getWidthByRate = rate => Math.floor(width * rate);
const getHeightByRate = rate => Math.floor(height * rate);
const scale = size => (width / guidelineBaseWidth) * size;
const scaleVertical = size => (height / guidelineBaseHeight) * size;

const colors = {
  white: '#ffffff',
  black: '#18191a',
  red: '#FF0000',
  transparent: 'transparent',
  aqua: '#04b38c',
  darkGray: '#9b9b9b',
  mediumPink: '#f2465b',
  tiffanyBlue: '#01a8b8',
  lightGrayishBlue: '#eff1f3',
  lightGrayishCyan: '#d8f6f8',
  backdrop: 'rgba(255, 255, 255, 0.7)',
  lightGray: '#d5d5d5',
};

const fonts = {
  sizes: {
    title1: 34,
    title2: 26,
    title3: 24,
    headline: 22,
    body: 20,
    button: 20,
    descriptions: 18,
    footnote: 15,
  },
  lineHeights: {
    title1: scale(48),
    title2: scale(37),
    title3: scale(33),
    headline: scale(30),
    body: scale(28),
    button: scale(28),
    descriptions: scale(25),
    footnote: scale(21),
  },
};

const styles = {
  container: {
    backgroundColor: colors.white,
    padding: 16,
  },
  text: {
    color: colors.black,
    fontSize: fonts.sizes.body,
    lineHeight: fonts.lineHeights.body,
  },
  button: {
    style: {
      borderRadius: 8,
      paddingVertical: 6,
      backgroundColor: colors.lightGrayishCyan,
      alignSelf: 'center',
      width: getWidthByRate(0.8),
      height: getHeightByRate(0.1),
    },
    container: {
      marginLeft: 0,
      marginRight: 0,
      marginTop: 16,
      marginBottom: 16,
      overflow: 'hidden',
      alignSelf: 'center',
    },
    text: {
      color: colors.tiffanyBlue,
      fontSize: fonts.sizes.button,
      fontWeight: 'normal',
    },
    disabled: {
      backgroundColor: colors.darkGray,
    },
    success: {
      backgroundColor: colors.aqua,
    },
    damage: {
      backgroundColor: colors.mediumPink,
    },
  },
  // List
  list: {
    style: {
      flexGrow: 1,
    },
    content: {
      marginTop: 12,
      marginBottom: 48,
    },
  },
  // Footer
  footer: {
    height: getHeightByRate(0.067),
  },
};

export default {
  deviceWidth: width,
  deviceHeight: height,
  adjustedWidth,
  adjustedHeight,
  pixelRatio,
  getWidthByRate,
  getHeightByRate,
  scale,
  scaleVertical,
  colors,
  fonts,
  styles,
};
