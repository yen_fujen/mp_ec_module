export default {
  Home: 'mp/home',
  SelectSN: 'mp/select_sn',
  DryTest: 'mp/dry_test',
  WetTest: 'mp/wet_test',
  ManualTests: 'mp/manual_tests',
  ScanQRCode: 'mp/scan_qr_code',
  Final: 'mp/final',
  FailedFinal: 'mp/failed_final',
};
