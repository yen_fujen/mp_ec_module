import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Views from './views';
import routes from './routes';

const MainNavigator = createStackNavigator(
  {
    [routes.Home]: {screen: Views.Home},
    [routes.SelectSN]: {screen: Views.SelectSN},
    [routes.DryTest]: {screen: Views.DryTest},
    [routes.WetTest]: {screen: Views.WetTest},
    [routes.ManualTests]: {screen: Views.ManualTests},
    [routes.ScanQRCode]: {screen: Views.ScanQRCode},
    [routes.Final]: {screen: Views.Final},
    [routes.FailedFinal]: {screen: Views.FailedFinal},
  },
  {
    headerMode: 'none',
  },
);

const App = createAppContainer(MainNavigator);

export default App;
