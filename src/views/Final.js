import React from 'react';
import PropTypes from 'prop-types';
import {SafeAreaView, View, Text, StyleSheet, Alert} from 'react-native';
import {Button} from 'react-native-elements';
import ReactNativeRestart from 'react-native-restart';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {Loading, NavStatePicker} from '../components';
import {theme, NavigationPropTypes} from '../configs';
import routes from '../routes';

@NavStatePicker
class Final extends React.PureComponent {
  static propTypes = {
    sn: PropTypes.string.isRequired,
    mpReport: PropTypes.arrayOf(PropTypes.shape({})),
    sendMpReport: PropTypes.func.isRequired,
    scannedSn: PropTypes.string,
    navigation: NavigationPropTypes,
  };

  constructor(props) {
    super(props);

    this.isSnDifferent = props.sn !== props.scannedSn;
    this.resultIcon = !this.isSnDifferent ? 'thumbs-o-up' : 'thumbs-o-down';
    this.scanIconColor = this.isSnDifferent
      ? theme.colors.tiffanyBlue
      : theme.colors.lightGray;
    this.compareColor = !this.isSnDifferent
      ? theme.colors.tiffanyBlue
      : theme.colors.mediumPink;
    this.hint = this.isSnDifferent ? 'SN 不相同，請重新掃描' : 'SN 相同';

    this.state = {loading: false};
  }

  onScanQRCode = () => {
    const {sn, mpReport} = this.props;
    this.props.navigation.navigate(routes.ScanQRCode, {sn, mpReport});
  };

  onUploadReport = async () => {
    this.setState({loading: true});
    try {
      await this.props.sendMpReport('success');
      this.setState({loading: false});
      Alert.alert('測試報告上傳成功', '測試結果: 全數通過測試項目，完成測試!', [
        {text: 'OK', onPress: () => ReactNativeRestart.Restart()},
      ]);
    } catch (err) {
      console.warn('[Error / Final] 上傳報告中發生失敗', err);
    }
  };

  renderLoading = () =>
    this.state.loading ? <Loading text={'測試報告上傳中...\n請稍候'} /> : null;

  render() {
    return (
      <SafeAreaView style={styles.wrap}>
        <View style={styles.container}>
          <Text style={styles.hintText}>
            請驗證機器序號是否正確，接著按下完成，結束測試
          </Text>

          <View style={styles.result}>
            <Text style={styles.prefixText}>目前機器序號:</Text>
            <Text style={styles.snText}>{this.props.sn}</Text>
            <Text style={styles.prefixText}>掃描取得的機器序號:</Text>
            <Text style={styles.snText}>{this.props.scannedSn}</Text>

            <View style={styles.row}>
              <FontAwesomeIcon
                name={this.resultIcon}
                size={30}
                style={styles.icon}
                color={this.compareColor}
              />
              <Text
                style={StyleSheet.flatten([
                  styles.compareText,
                  {color: this.compareColor},
                ])}>
                {this.hint}
              </Text>
            </View>
          </View>

          <View style={styles.footer}>
            <Button
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
              titleStyle={styles.buttonText}
              disabledStyle={styles.buttonDisabled}
              onPress={this.onScanQRCode}
              disabled={!this.isSnDifferent}
              icon={
                <FontAwesomeIcon
                  name="qrcode"
                  size={28}
                  color={this.scanIconColor}
                />
              }
              title="掃描 QR Code"
            />

            <Button
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
              titleStyle={styles.buttonText}
              disabledStyle={styles.buttonDisabled}
              disabled={this.isSnDifferent}
              onPress={this.onUploadReport}
              title="完成"
            />
          </View>
        </View>
        {this.renderLoading()}
      </SafeAreaView>
    );
  }
}

export default Final;

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
  },
  container: {
    flexGrow: 1,
    justifyContent: 'space-between',
    ...theme.styles.container,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleText: {
    ...theme.styles.text,
    fontSize: theme.fonts.sizes.title2,
    marginBottom: 16,
  },
  resultText: {
    ...theme.styles.text,
    fontSize: theme.fonts.sizes.title1,
    color: theme.colors.tiffanyBlue,
  },
  result: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  hintText: {
    ...theme.styles.text,
    fontWeight: 'bold',
    fontSize: theme.fonts.sizes.descriptions,
    lineHeight: theme.fonts.lineHeights.descriptions,
    textAlign: 'center',
  },
  prefixText: {
    ...theme.styles.text,
    fontSize: theme.fonts.sizes.title2,
  },
  snText: {
    ...theme.styles.text,
    fontSize: 40,
    lineHeight: 48,
  },
  compareText: {
    ...theme.styles.text,
    fontSize: theme.fonts.sizes.title1,
    fontWeight: 'bold',
    lineHeight: theme.fonts.lineHeights.title1,
    marginTop: 10,
  },
  buttonContainer: {
    ...theme.styles.button.container,
  },
  button: {
    ...theme.styles.button.style,
  },
  buttonText: {
    ...theme.styles.button.text,
    fontSize: theme.fonts.sizes.title2,
    marginLeft: 8,
  },
  buttonDisabled: {
    ...theme.styles.buttonDisabled,
  },
  icon: {
    marginTop: 20,
    marginRight: 8,
  },
  footer: {
    marginLeft: 15,
    marginRight: 15,
  },
});
