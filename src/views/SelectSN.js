import React from 'react';
import PropTypes from 'prop-types';
import {
  SafeAreaView,
  StyleSheet,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import CONFIG from 'react-native-config';
import {isNil} from 'ramda';
import {orderBy} from 'lodash';
import eko from '../services/eko';
import udp from '../services/udp';
import {throttler} from '../helpers';
import {Loading, SNBtn} from '../components';
import {theme} from '../configs';
import routes from '../routes';

const BROADCAST_ALL_ESP = 'HiEKM;a;';

export default class SelectSN extends React.PureComponent {
  static propTypes = {
    action: PropTypes.oneOf(['skip', 'auto']),
    navigation: PropTypes.shape({
      navigate: PropTypes.func.isRequired,
      goBack: PropTypes.func.isRequired,
      reset: PropTypes.func.isRequired,
    }),
  };

  constructor(props) {
    super(props);

    this.state = {
      connecting: false,
      modules: [],
    };

    eko.open();

    try {
      // 建立 EventEmmit 事件監聽
      // http://www.runoob.com/nodejs/nodejs-event.html
      udp.on('message', this.udpHandler);
    } catch (err) {
      console.warn('[Error / SelectSn / udp on]', err);
    }
  }

  componentDidMount() {
    this.timer = setInterval(() => udp.write(BROADCAST_ALL_ESP), 1000);

    udp
      .open()
      .then(() => this.timer)
      .catch(err => console.warn('[Error / udp listener]', err));
  }

  componentWillUnmount() {
    this.closeUDP();
  }

  // EventEmmit 的事件處理，等待 UDP 找到然後回應，再顯示
  udpHandler = device => {
    if (!this.state.modules.some(module => module.sn === device.sn)) {
      const modules = this.state.modules;
      const newModules = modules.concat(device);
      // 將還沒有完成配對的機器移到最上方
      orderBy(newModules, ['paired'], ['desc']);
      this.setState({modules: newModules});
    }
  };

  closeUDP = () => {
    if (!isNil(udp)) {
      udp.removeListener('message', this.udpHandler);
      udp.close();
      this.timer && clearInterval(this.timer);
      this.timer = null;
    }
  };

  onPressTank = throttler(({sn, ip}) => {
    this.setState({connecting: true});

    const device = {
      sn,
      host: ip,
      key: CONFIG.PERIPHERAL_KEY,
      iv: CONFIG.PERIPHERAL_IV,
    };
    eko
      .connect(device)
      .then(() => {
        this.setState({connecting: false});
        this.closeUDP();

        const params = {
          sn,
          mpReport: [],
        };

        this.props.navigation.navigate(routes.DryTest, params);
      })
      .catch(error => {
        console.warn('[Error] 連線到 EC Module 時發生錯誤:', error);
        this.setState({connecting: false});
      });
  });

  renderSNBtn = ({item}) =>
    !isNil(item.ip) && (
      <SNBtn
        sn={item.sn}
        paired={item.paired}
        mac={item.mac}
        ip={item.ip}
        onPress={this.onPressTank}
      />
    );

  renderLoading = () => <Loading text={'機器連線中...\n請稍候'} />;

  /**
   * 繪製畫面
   */
  render() {
    return (
      <SafeAreaView style={styles.wrap}>
        <FlatList
          keyExtractor={item => item.sn.toString()}
          data={this.state.modules}
          style={styles.container}
          contentContainerStyle={styles.content}
          renderItem={this.renderSNBtn}
          ListFooterComponent={
            <ActivityIndicator color={theme.colors.tiffanyBlue} size="small" />
          }
        />

        {this.state.connecting && this.renderLoading()}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    padding: 16,
  },
  container: {
    flexGrow: 1,
  },
  content: {
    ...theme.styles.container,
  },
});
