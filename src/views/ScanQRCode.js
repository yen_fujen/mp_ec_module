import React from 'react';
import PropTypes from 'prop-types';
import {View, Text, StyleSheet, SafeAreaView, Platform} from 'react-native';
import {RESULTS} from 'react-native-permissions';
import {QRCodeScanner} from '../components';
import {
  confirmCameraPermission,
  checkCameraPermission,
  requestCameraPermission,
} from '../helpers';
import {NavStatePicker} from '../components';
import {theme, NavigationPropTypes} from '../configs';
import routes from '../routes';

@NavStatePicker
class ScanQRCode extends React.PureComponent {
  static propTypes = {
    sn: PropTypes.string.isRequired,
    mpReport: PropTypes.arrayOf(PropTypes.shape({})),
    navigation: NavigationPropTypes,
  };

  state = {
    isCameraReady: false,
    canReadBarCode: true,
  };

  componentDidMount() {
    const result = this.checkPermission[Platform.OS]();
    if (result !== RESULTS.GRANTED) {
      return this.handleRequestPermission();
    }
    this.updateCameraStatus();
  }

  checkPermission = {
    ios: confirmCameraPermission,
    android: checkCameraPermission,
  };

  updateCameraStatus = permission => {
    this.setState({isCameraReady: permission === RESULTS.GRANTED});
  };

  handleRequestPermission = () =>
    requestCameraPermission().then(this.updateCameraStatus);

  scanSuccessHandler = async scannedSn => {
    this.props.navigation.navigate(routes.Final, {
      sn: this.props.sn,
      mpReport: this.props.mpReport,
      scannedSn,
    });
  };

  render() {
    return (
      <SafeAreaView style={styles.wrap}>
        <QRCodeScanner
          onSuccess={this.scanSuccessHandler}
          isCameraReady={this.state.isCameraReady}
          canReadBarCode={this.state.canReadBarCode}
        />

        <View style={styles.footer}>
          <Text style={styles.readmeText}>請掃描對應機器的 QR Code 標籤</Text>
        </View>
      </SafeAreaView>
    );
  }
}

export default ScanQRCode;

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
  },
  footer: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    width: theme.deviceWidth,
    height: 70,
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 25,
    backgroundColor: 'rgba(173, 237, 241, 0.5)',
  },
  readmeText: {
    fontSize: theme.fonts.sizes.descriptions,
    color: theme.colors.white,
    textAlign: 'center',
  },
});
