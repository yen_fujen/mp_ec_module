import Home from './Home';
import SelectSN from './SelectSN';
import DryTest from './DryTest';
import WetTest from './WetTest';
import ManualTests from './ManualTests';
import ScanQRCode from './ScanQRCode';
import Final from './Final';
import FailedFinal from './FailedFinal';

export default {
  Home,
  SelectSN,
  DryTest,
  WetTest,
  ManualTests,
  ScanQRCode,
  Final,
  FailedFinal,
};
