import React from 'react';
import PropTypes from 'prop-types';
import {SafeAreaView, View, Text, StyleSheet} from 'react-native';
import {Button, CheckBox} from 'react-native-elements';
import {isNil} from 'ramda';
import {NavStatePicker} from '../components';
import {theme, NavigationPropTypes} from '../configs';
import routes from '../routes';

const LED_TESTS = [
  {
    title: '關閉 WiFi, Status LED',
    key: 'testLightsOff',
  },
  {
    title: '開啟 WiFi, Status LED',
    key: 'testLightsOn',
  },
];

const ALL_TESTS = ['testLightsOn', 'testLightsOff', 'resetButton'];

@NavStatePicker
class ManualTests extends React.PureComponent {
  static propTypes = {
    sn: PropTypes.string.isRequired,
    mpReport: PropTypes.arrayOf(PropTypes.shape({})),
    mpRequest: PropTypes.func.isRequired,
    navigation: NavigationPropTypes,
  };

  state = {
    testLightsOn: null,
    testLightsOnRes: null,
    testLightsOff: null,
    testLightsOffRes: null,
    resetButton: null,
  };

  renderCheckBoxes = key => {
    return (
      <View style={styles.checkBoxes}>
        <CheckBox
          title="成功"
          textStyle={StyleSheet.flatten([styles.checkBoxTitle, styles.success])}
          containerStyle={styles.checkBoxContainer}
          checked={this.state[key]}
          onPress={() => this.setState({[key]: true})}
        />
        <CheckBox
          right
          title="失敗"
          textStyle={StyleSheet.flatten([styles.checkBoxTitle, styles.fail])}
          containerStyle={styles.checkBoxContainer}
          checked={isNil(this.state[key]) ? false : !this.state[key]}
          onPress={() => this.setState({[key]: false})}
        />
      </View>
    );
  };

  renderLEDTests = () => {
    return LED_TESTS.map(({title, key}) => {
      return (
        <React.Fragment key={key}>
          <Button
            containerStyle={styles.buttonContainer}
            buttonStyle={StyleSheet.flatten([styles.button, styles.mb20])}
            titleStyle={styles.buttonText}
            title={title}
            onPress={() => this.onPressTest(key)}
          />
          {this.renderCheckBoxes(key)}
        </React.Fragment>
      );
    });
  };

  onPressTest = async key => {
    // action 3 燈全開, action 4 燈全關
    const action = key === 'testLightsOff' ? 4 : 3;
    try {
      const res = await this.props.mpRequest(action);
      this.setState({[`${key}Res`]: res});
    } catch (err) {
      console.warn('[Error / LED Test]', err);
    }
  };

  onPressNext = () => {
    let isAnyManualTestFail = false;
    ALL_TESTS.forEach(test => {
      this.props.mpReport.push({
        name: test,
        result: this.state[test] ? 'success' : 'fail',
        response: test !== 'resetButton' ? this.state[`${test}Res`] : null,
      });
      isAnyManualTestFail = isAnyManualTestFail || !this.state[test];
    });
    const nextPage =
      this.props.haveAnyTestsFailed || isAnyManualTestFail
        ? routes.FailedFinal
        : routes.ScanQRCode;

    this.props.navigation.navigate(nextPage, {
      sn: this.props.sn,
      mpReport: this.props.mpReport,
      // 如果上一步有發生錯誤 this.props.haveAnyTestsFailed 會是 true, 則直接將 true 傳下去
      // haveAnyTestsFailed: this.props.haveAnyTestsFailed || isAnyManualTestFail,
    });
  };

  render() {
    const isEveryTestDone = Object.values(this.state).every(v => !isNil(v));
    return (
      <SafeAreaView style={styles.base}>
        <View>
          <Text style={styles.title}>LED 測試</Text>
          {this.renderLEDTests()}
          <View style={styles.separator} />
          <Text style={styles.title}>重置測試</Text>
          {this.renderCheckBoxes('resetButton')}
        </View>
        <Button
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.button}
          titleStyle={styles.buttonText}
          title="下一步"
          disabled={!isEveryTestDone}
          onPress={this.onPressNext}
        />
      </SafeAreaView>
    );
  }
}

export default ManualTests;

const styles = StyleSheet.create({
  base: {
    flex: 1,
    ...theme.styles.container,
    justifyContent: 'space-between',
  },
  title: {
    ...theme.styles.text,
    fontSize: theme.fonts.sizes.title3,
    fontWeight: 'bold',
    textAlign: 'center',
    // marginBottom: 24,
  },
  text: {
    ...theme.styles.text,
    fontWeight: 'bold',
    paddingLeft: 16,
    paddingRight: 16,
    marginBottom: 16,
  },
  buttonContainer: {
    ...theme.styles.button.container,
  },
  button: {
    ...theme.styles.button.style,
  },
  buttonText: {
    ...theme.styles.button.text,
    fontSize: theme.fonts.sizes.title3,
  },
  checkBoxes: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  checkBoxContainer: {
    backgroundColor: 'transparent',
    borderWidth: 0,
  },
  checkBoxTitle: {
    ...theme.styles.text,
    fontSize: theme.fonts.sizes.title3,
  },
  success: {
    color: theme.colors.aqua,
  },
  fail: {
    color: theme.colors.mediumPink,
  },
  separator: {
    height: StyleSheet.hairlineWidth,
    width: '100%',
    backgroundColor: theme.colors.darkGray,
    marginTop: 16,
    marginBottom: 16,
  },
});
