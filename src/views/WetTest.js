import React from 'react';
import PropTypes from 'prop-types';
import {SafeAreaView, View, Text, StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';
import {isEmpty} from 'ramda';
import {Loading, NavStatePicker} from '../components';
import {theme, NavigationPropTypes} from '../configs';
import routes from '../routes';

@NavStatePicker
class WetTest extends React.PureComponent {
  static propTypes = {
    sn: PropTypes.string.isRequired,
    mpReport: PropTypes.arrayOf(PropTypes.shape({})),
    mpRequest: PropTypes.func.isRequired,
    haveAnyTestsFailed: PropTypes.bool.isRequired,
    navigation: NavigationPropTypes,
  };

  state = {
    deviation: '',
    tempResult: '',
    temperature: '',
    disableNext: true,
    loading: false,
    isDeviationValid: false,
    isTemperatureValid: false,
  };

  emphasizeStyle = (isValid = true) => {
    const color = isValid ? 'success' : 'fail';
    return StyleSheet.flatten([styles.emphasize, styles[color]]);
  };

  onPressNext = () => {
    const {isDeviationValid, isTemperatureValid} = this.state;
    this.props.navigation.navigate(routes.ManualTests, {
      sn: this.props.sn,
      mpReport: this.props.mpReport,
      // 如果上一步有發生錯誤 this.props.haveAnyTestsFailed 會是 true, 則直接將 true 傳下去
      haveAnyTestsFailed:
        this.props.haveAnyTestsFailed ||
        !isDeviationValid ||
        !isTemperatureValid,
    });
  };

  onPressTest = async () => {
    this.setState({loading: true});
    try {
      const res = await this.props.mpRequest(2);
      // response 直接塞入 mpReport
      this.props.mpReport.push(res);
      const {
        data: {deviation, temperature, err_temperature},
      } = res;
      // 濕校正 deviation 合理範圍 1 ~ 3
      const isDeviationValid = +deviation >= 1 && +deviation <= 3;
      // 溫度若為正常，err_temperature 返回的訊息就會是 'NONE'
      const isTemperatureValid = err_temperature === 'NONE';
      this.setState({
        deviation,
        temperature,
        isDeviationValid,
        isTemperatureValid,
        tempResult: err_temperature,
        disableNext: false,
      });
    } catch (err) {
      console.warn('[Error] 水質偵測', err);
    }
    this.setState({loading: false});
  };

  renderResult = () => {
    const {
      deviation,
      tempResult,
      temperature,
      isDeviationValid,
      isTemperatureValid,
    } = this.state;
    const temperatureMsg = isTemperatureValid ? '正常' : tempResult;
    // 溫度沒有定合理範圍，25 ~ 30 這個範圍是跟 Rita 口頭上定義的，因為我們認為正常是溫差不多介於這之間
    // 因此不論溫度幾度，只要 err_temperature 返回 'NONE' 都算溫度孔位正常
    const isTempReasonable = +temperature >= 25 && +temperature <= 30;
    return (
      <React.Fragment>
        <Text style={styles.text}>
          鹽度正常範圍：
          <Text style={styles.green}>1 ~ 3</Text>
        </Text>
        <Text style={styles.text}>
          鹽度測量結果(Deviation)：
          <Text style={this.emphasizeStyle(isDeviationValid)}>{deviation}</Text>
        </Text>
        <View style={styles.separator} />
        <Text style={styles.text}>
          溫度測量結果：
          <Text style={this.emphasizeStyle(isTempReasonable)}>
            {temperature}
          </Text>
        </Text>
        <Text style={styles.text}>
          溫度孔位偵測：
          <Text style={this.emphasizeStyle(isTemperatureValid)}>
            {temperatureMsg}
          </Text>
        </Text>
      </React.Fragment>
    );
  };

  renderLoading = () => <Loading text={'水質偵測中...\n請稍候'} />;

  render() {
    return (
      <SafeAreaView style={styles.base}>
        <View>
          <Text style={styles.title}>水質偵測</Text>
          <Text style={styles.text}>
            請確認 EC Module
            <Text style={this.emphasizeStyle()}> 有插 </Text>
            <Text>鹽度和溫度探棒</Text>
          </Text>
          <Button
            containerStyle={styles.buttonContainer}
            buttonStyle={StyleSheet.flatten([styles.button, styles.mb20])}
            titleStyle={styles.buttonText}
            onPress={this.onPressTest}
            title="執行測試"
          />
          {(!isEmpty(this.state.deviation) ||
            !isEmpty(this.state.tempResult)) &&
            this.renderResult()}
        </View>
        <Button
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.button}
          titleStyle={styles.buttonText}
          onPress={this.onPressNext}
          disabled={this.state.disableNext}
          title="下一步"
        />
        {this.state.loading && this.renderLoading()}
      </SafeAreaView>
    );
  }
}

export default WetTest;

const styles = StyleSheet.create({
  base: {
    flex: 1,
    ...theme.styles.container,
    justifyContent: 'space-between',
  },
  title: {
    ...theme.styles.text,
    fontSize: theme.fonts.sizes.title3,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 24,
  },
  text: {
    ...theme.styles.text,
    fontWeight: 'bold',
    paddingLeft: 16,
    paddingRight: 16,
    marginBottom: 16,
  },
  buttonContainer: {
    ...theme.styles.button.container,
  },
  button: {
    ...theme.styles.button.style,
  },
  buttonText: {
    ...theme.styles.button.text,
    fontSize: theme.fonts.sizes.title2,
  },
  emphasize: {
    fontSize: theme.fonts.sizes.title3,
  },
  separator: {
    height: StyleSheet.hairlineWidth,
    width: '100%',
    backgroundColor: theme.colors.darkGray,
    marginBottom: 16,
  },
  success: {
    color: theme.colors.aqua,
  },
  fail: {
    color: theme.colors.mediumPink,
  },
  mb20: {
    marginBottom: 20,
  },
});
