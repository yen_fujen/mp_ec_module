import React from 'react';
import PropTypes from 'prop-types';
import {SafeAreaView, View, Text, StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';
import {isNil} from 'ramda';
import {Loading, NavStatePicker} from '../components';
import {theme, NavigationPropTypes} from '../configs';
import routes from '../routes';

@NavStatePicker
class DryTest extends React.PureComponent {
  static propTypes = {
    sn: PropTypes.string.isRequired,
    mpReport: PropTypes.arrayOf(PropTypes.shape({})),
    mpRequest: PropTypes.func.isRequired,
    navigation: NavigationPropTypes,
  };

  state = {
    oscv: null,
    disableNext: true,
    loading: false,
    isOSCVValid: false,
  };

  emphasizeStyle = (isValid = true) => {
    const color = isValid ? 'success' : 'fail';
    return StyleSheet.flatten([styles.emphasize, styles[color]]);
  };

  onPressTest = async () => {
    this.setState({loading: true});
    try {
      const res = await this.props.mpRequest(1);
      // response 直接塞入 mpReport
      this.props.mpReport.push(res);
      // 乾校正 oscv 合理範圍 45 ~ 65
      const isOSCVValid = res.data.oscv >= 45 && res.data.oscv <= 65;
      this.setState({
        oscv: res.data.oscv,
        disableNext: false,
        loading: false,
        isOSCVValid,
      });
    } catch (err) {
      console.warn('[Error] 空機電壓測試', err);
    }
  };

  onPressNext = () => {
    this.props.navigation.navigate(routes.WetTest, {
      sn: this.props.sn,
      mpReport: this.props.mpReport,
      haveAnyTestsFailed: !this.state.isOSCVValid,
    });
  };

  renderResult = () => {
    const {isOSCVValid} = this.state;
    const result = isOSCVValid ? ' 成功' : ' 失敗';
    return (
      <React.Fragment>
        <Text style={styles.text}>
          測試結果：
          <Text style={this.emphasizeStyle(isOSCVValid)}>{result}</Text>
        </Text>
        <Text style={styles.text}>
          oscv 正常範圍：
          <Text style={styles.green}>45 ~ 65</Text>
        </Text>
        <Text style={styles.text}>
          測得 oscv：
          <Text style={this.emphasizeStyle(isOSCVValid)}>
            {this.state.oscv}
          </Text>
        </Text>
      </React.Fragment>
    );
  };

  renderLoading = () => <Loading text={'空機電壓測試中...\n請稍候'} />;

  render() {
    const isOSCVNil = !isNil(this.state.oscv);
    return (
      <SafeAreaView style={styles.base}>
        <View>
          <Text style={styles.title}>空機電壓測試</Text>
          <Text style={styles.text}>
            請確認 EC Module
            <Text style={this.emphasizeStyle()}> 沒有 </Text>
            <Text>插鹽度和溫度探棒</Text>
          </Text>
          <Button
            containerStyle={styles.buttonContainer}
            buttonStyle={StyleSheet.flatten([styles.button, styles.mb20])}
            titleStyle={styles.buttonText}
            onPress={this.onPressTest}
            title="執行測試"
          />
          {isOSCVNil && this.renderResult()}
        </View>
        <Button
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.button}
          titleStyle={styles.buttonText}
          onPress={this.onPressNext}
          disabled={this.state.disableNext}
          title="下一步"
        />
        {this.state.loading && this.renderLoading()}
      </SafeAreaView>
    );
  }
}

export default DryTest;

const styles = StyleSheet.create({
  base: {
    flex: 1,
    ...theme.styles.container,
    justifyContent: 'space-between',
  },
  title: {
    ...theme.styles.text,
    fontSize: theme.fonts.sizes.title3,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 24,
  },
  text: {
    ...theme.styles.text,
    fontWeight: 'bold',
    paddingLeft: 16,
    paddingRight: 16,
    marginBottom: 16,
  },
  buttonContainer: {
    ...theme.styles.button.container,
  },
  button: {
    ...theme.styles.button.style,
  },
  buttonText: {
    ...theme.styles.button.text,
    fontSize: theme.fonts.sizes.title2,
  },
  emphasize: {
    fontSize: theme.fonts.sizes.title3,
  },
  success: {
    color: theme.colors.aqua,
  },
  fail: {
    color: theme.colors.mediumPink,
  },
  mb20: {
    marginBottom: 20,
  },
});
