import React from 'react';
import {SafeAreaView, View, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Button} from 'react-native-elements';
import theme from '../configs/theme';
import routes from '../routes';

const Home = props => {
  const onPressMP = () => props.navigation.navigate(routes.SelectSN);

  const icon = (
    <Icon name="assignment" size={25} color={theme.colors.tiffanyBlue} />
  );

  return (
    <SafeAreaView style={styles.base}>
      <View>
        <Text
          style={StyleSheet.flatten([styles.title, styles.mt40, styles.mb20])}>
          eKoPro
        </Text>
        <Text style={StyleSheet.flatten([styles.title, styles.mb30])}>
          EC Module
        </Text>
        <Button
          icon={icon}
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.button}
          titleStyle={styles.buttonText}
          onPress={onPressMP}
          title="開始 MP 測試"
        />
      </View>
      <Text style={styles.signature}>© 2019 eKoPro Solutions</Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  base: {
    flex: 1,
    ...theme.styles.container,
    justifyContent: 'space-between',
  },
  title: {
    ...theme.styles.text,
    color: theme.colors.black,
    fontSize: theme.fonts.sizes.title1,
    fontWeight: 'bold',
    lineHeight: theme.fonts.lineHeights.title3,
    textAlign: 'center',
  },
  buttonContainer: {
    ...theme.styles.button.container,
  },
  button: {
    ...theme.styles.button.style,
  },
  buttonText: {
    ...theme.styles.button.text,
    fontSize: theme.fonts.sizes.title2,
    marginLeft: 8,
  },
  signature: {
    textAlign: 'center',
  },
  mt40: {
    marginTop: 40,
  },
  mb20: {
    marginBottom: 20,
  },
  mb30: {
    marginBottom: 30,
  },
});

export default React.memo(Home);
