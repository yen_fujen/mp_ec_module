import React from 'react';
import PropTypes from 'prop-types';
import {SafeAreaView, View, Text, StyleSheet, Alert} from 'react-native';
import {Button} from 'react-native-elements';
import ReactNativeRestart from 'react-native-restart';
import {Loading, NavStatePicker} from '../components';
import {theme, NavigationPropTypes} from '../configs';

@NavStatePicker
class Final extends React.PureComponent {
  static propTypes = {
    sn: PropTypes.string.isRequired,
    sendMpReport: PropTypes.func.isRequired,
    navigation: NavigationPropTypes,
  };

  state = {loading: false};

  onUploadReport = async () => {
    this.setState({loading: true});
    try {
      await this.props.sendMpReport('fail');
      this.setState({loading: false});
      Alert.alert('測試報告上傳成功', '', [
        {text: 'OK', onPress: () => ReactNativeRestart.Restart()},
      ]);
    } catch (err) {
      console.warn('[Error / Final] 上傳報告中發生失敗', err);
    }
  };

  renderLoading = () =>
    this.state.loading ? <Loading text={'測試報告上傳中...\n請稍候'} /> : null;

  render() {
    return (
      <SafeAreaView style={styles.wrap}>
        <View>
          <Text style={StyleSheet.flatten([styles.title, styles.mb20])}>
            未能完全通過測試，有部分項目發生測試失敗!
          </Text>
          <Text style={styles.title}>按下完成，結束測試!</Text>
        </View>

        <View style={styles.result}>
          <Text style={styles.prefixText}>目前機器序號:</Text>
          <Text style={styles.snText}>{this.props.sn}</Text>
        </View>

        <Button
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.button}
          titleStyle={styles.buttonText}
          disabledStyle={styles.buttonDisabled}
          onPress={this.onUploadReport}
          title="完成"
        />
        {this.renderLoading()}
      </SafeAreaView>
    );
  }
}

export default Final;

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    justifyContent: 'space-between',
    ...theme.styles.container,
    backgroundColor: '#282c34',
  },
  result: {
    alignItems: 'center',
  },
  title: {
    ...theme.styles.text,
    fontSize: theme.fonts.sizes.headline,
    textAlign: 'center',
    color: theme.colors.white,
  },
  prefixText: {
    ...theme.styles.text,
    fontSize: theme.fonts.sizes.title2,
    color: theme.colors.white,
  },
  snText: {
    ...theme.styles.text,
    fontSize: 40,
    lineHeight: 48,
    color: theme.colors.white,
  },
  buttonContainer: {
    ...theme.styles.button.container,
  },
  button: {
    ...theme.styles.button.style,
    backgroundColor: theme.colors.white,
  },
  buttonText: {
    ...theme.styles.button.text,
    fontSize: theme.fonts.sizes.title2,
    color: '#282c34',
  },
  buttonDisabled: {
    ...theme.styles.buttonDisabled,
  },
  mb20: {
    marginBottom: 20,
  },
});
